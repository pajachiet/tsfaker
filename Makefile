install:
	pip3 install -e ".[dev]"

test:
	py.test tests

deploy:
	python3 -m pip install --upgrade twine
	python3 setup.py sdist bdist_wheel
	twine upload dist/*

.PHONY: install test
