import os
import shutil
import pandas as pd

import pytest
from click.testing import CliRunner
from tableschema import Schema

from tsfaker.exceptions import DifferentNumberInputOutput, ResourceMissing, ResourceConflict, ResourceCycle, \
    InvalidLoggingLevel
from tsfaker.main import cli

implemented_types_json = os.path.join('tests', 'schemas', 'implemented_types.json')

parent_child_directory = os.path.join('tests', 'schemas', 'parent_child')
child1_json = os.path.join(parent_child_directory, 'child1.json')
child2_json = os.path.join(parent_child_directory, 'child2.json')
parent_json = os.path.join(parent_child_directory, 'parent.json')

cycle_directory = os.path.join('tests', 'schemas', 'cycle')
brother1_json = os.path.join(cycle_directory, 'brother1.json')
brother2_json = os.path.join(cycle_directory, 'brother2.json')

csv_directory = os.path.join('tests', 'csv')
parent_csv = os.path.join(csv_directory, 'parent.csv')

tests_tmp_directory = os.path.join('tests', 'tmp')
parent_tmp_csv = os.path.join(tests_tmp_directory, 'parent.csv')

runner = CliRunner()


def test_stdin_input():
    with open(parent_json, 'r') as f:
        stdin = f.read()
        runner.invoke(cli, '-', input=stdin, catch_exceptions=False)


def test_non_default_separator():
    if os.path.exists(tests_tmp_directory):
        shutil.rmtree(tests_tmp_directory)
    os.mkdir(tests_tmp_directory)
    runner.invoke(cli, '{} -o {} --separator ;'.format(parent_json, parent_tmp_csv), catch_exceptions=False)
    expected_columns = Schema(parent_json).field_names
    actual_columns = list(pd.read_csv(parent_tmp_csv, sep=';').columns)
    assert expected_columns == actual_columns
    shutil.rmtree(tests_tmp_directory)


def test_different_number_of_output():
    result = runner.invoke(cli, '- -o tmp.csv -o tmp2.csv --dry-run')
    assert isinstance(result.exception, DifferentNumberInputOutput)

    result = runner.invoke(cli, '{} {} -o tmp.csv --dry-run'.format(parent_json, implemented_types_json))
    assert isinstance(result.exception, DifferentNumberInputOutput)

    runner.invoke(cli, '{} {} -o - -o tmp.csv --dry-run'.format(parent_json, implemented_types_json),
                  catch_exceptions=False)


def test_foreign_keys():
    result = runner.invoke(cli, child1_json)
    assert isinstance(result.exception, ResourceMissing)

    runner.invoke(cli, '{} --ignore-foreign-keys'.format(child1_json), catch_exceptions=False)
    runner.invoke(cli, '{} -r {}'.format(child1_json, parent_csv), catch_exceptions=False)
    runner.invoke(cli, '{} -r {}'.format(child2_json, parent_csv), catch_exceptions=False)
    runner.invoke(cli, '{} -r {}'.format(child1_json, csv_directory), catch_exceptions=False)

    if os.path.exists(tests_tmp_directory):
        shutil.rmtree(tests_tmp_directory)
    os.mkdir(tests_tmp_directory)
    runner.invoke(cli, '{} -o {}'.format(parent_child_directory, tests_tmp_directory), catch_exceptions=False)

    shutil.rmtree(tests_tmp_directory)
    os.mkdir(tests_tmp_directory)
    runner.invoke(cli, '{} {} -o {} -o -'.format(parent_json, child1_json, parent_tmp_csv), catch_exceptions=False)
    shutil.rmtree(tests_tmp_directory)


def test_foreign_keys_creation_order():
    if os.path.exists(tests_tmp_directory):
        shutil.rmtree(tests_tmp_directory)
    os.mkdir(tests_tmp_directory)
    runner.invoke(cli, '{} {} -o - -o {}'.format(child1_json, parent_json, parent_tmp_csv), catch_exceptions=False)
    shutil.rmtree(tests_tmp_directory)


@pytest.mark.timeout(1)
def test_foreign_keys_cycle():
    result = runner.invoke(cli, '{} {}'.format(brother1_json, brother2_json))
    assert isinstance(result.exception, ResourceCycle)


def test_conflicting_resources():
    result = runner.invoke(cli, '{} -r {} -r {}'.format(child1_json, parent_csv, parent_csv))
    assert isinstance(result.exception, ResourceConflict)


def test_logging_level():
    for logging_level in ['CRITICAL', 'ERROR', 'WARN', 'WARNING', 'INFO', 'DEBUG', 'NOTSET']:
        runner.invoke(cli, '--logging-level {}'.format(logging_level), catch_exceptions=False)

    result = runner.invoke(cli, '--logging-level {}'.format('not a logging level'))
    assert isinstance(result.exception, InvalidLoggingLevel)
