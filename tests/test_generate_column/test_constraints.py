import numpy as np
import pytest

from tsfaker.generator import column


# Bounded
@pytest.mark.parametrize('minLength,maxLength', [(2, 2), (2, 3),
                                                 (column.String.DEFAULT_MAX_LENGTH + 1, None)
                                                 ])
def test_generate_string_bounded(minLength, maxLength):
    # Given
    column_generator = column.String(nrows=100, minLength=minLength, maxLength=maxLength)

    # When
    array = column_generator._get_1d_array()

    # Then
    for _, value in np.ndenumerate(array):
        assert len(value) >= column_generator.minLength
        assert len(value) <= column_generator.maxLength


@pytest.mark.parametrize('minimum,maximum', [(2, 2), (2, 3),
                                             (column.Integer.DEFAULT_MAXIMUM + 1, None),
                                             (None, column.Integer.DEFAULT_MINIMUM - 1)
                                             ])
def test_generate_integer_bounded(minimum, maximum):
    # Given
    column_generator = column.Integer(nrows=100, minimum=minimum, maximum=maximum)

    # When
    array = column_generator._get_1d_array()
    array = array.astype(np.int64)
    # Then
    assert (array == column_generator.minimum).any()  # Probability false = 1/2^nrows
    assert (array == column_generator.maximum).any()  # Probability false = 1/2^nrows
    assert np.greater_equal(array, column_generator.minimum).all()
    assert np.less_equal(array, column_generator.maximum).all()


@pytest.mark.parametrize('minimum,maximum', [(2, 2), (2, 3),
                                             (column.Integer.DEFAULT_MAXIMUM + 1, None),
                                             (None, column.Integer.DEFAULT_MINIMUM - 1)
                                             ])
def test_generate_number_bounded(minimum, maximum):
    # Given
    column_generator = column.Number(nrows=100, minimum=minimum, maximum=maximum)

    # When
    array = column_generator._get_1d_array()

    # Then
    assert np.greater_equal(array, column_generator.minimum).all()
    assert np.less_equal(array, column_generator.maximum).all()


@pytest.mark.parametrize('minimum,maximum', [("2000-01-01T00:00:00", "2001-01-01T00:00:01"),
                                             ("2000-01-01", "2001-01-01"),
                                             ("2000-01-01", "2000-01-01")])
def test_generate_datetime_bounded(minimum, maximum):
    # Given
    column_generator = column.Datetime(nrows=100, minimum=minimum, maximum=maximum)
    date_minimum = np.datetime64(minimum)
    date_maximum = np.datetime64(maximum)

    # When
    array = column_generator._get_1d_array()
    date_array = np.array(list(map(np.datetime64, array)))

    # Then
    assert np.greater_equal(date_array, date_minimum).all()
    assert np.less_equal(date_array, date_maximum).all()


@pytest.mark.parametrize('minimum,maximum, format_date', [("2000-01-01", "2000-01-02", "%Y-%m-%d"),
                                             ("2000-01-01", "2000-01-01", "%Y-%m-%d"),
                                             ("2100-12-29", None, "%Y-%m-%d"),
                                             (None, "1800-01-01", "%Y-%m-%d")
                                             ])
def test_generate_date_bounded(minimum, maximum, format_date):
    # Given
    column_generator = column.Date(nrows=100, minimum=minimum, maximum=maximum, format=format_date)

    # When
    array = column_generator._get_1d_array()
    date_array = np.array(list(map(np.datetime64, array)))

    # Then
    assert np.greater_equal(date_array, column_generator.minimum).all()
    assert np.less_equal(date_array, column_generator.maximum).all()
    assert (date_array == column_generator.minimum).any()  # Probability false = 1/2^nrows
    assert (date_array == column_generator.maximum).any()  # Probability false = 1/2^nrows


@pytest.mark.parametrize('minimum,maximum', [("2000-01", "2000-02"),
                                             ("2000-01-01", "2000-02-01"),
                                             ("2000-01-01", "2000-01-01")])
def test_generate_yearmonth_bounded(minimum, maximum):
    # Given
    column_generator = column.Yearmonth(nrows=100, minimum=minimum, maximum=maximum)
    date_minimum = np.datetime64(minimum.replace("-", "")[0:6])
    date_maximum = np.datetime64(maximum.replace("-", "")[0:6])

    # When
    array = column_generator._get_1d_array()
    date_array = np.array(list(map(np.datetime64, array)))
    print(array)

    # Then
    assert np.greater_equal(date_array, date_minimum).all()
    assert np.less_equal(date_array, date_maximum).all()
    assert (date_array == date_minimum).any()  # Probability false = 1/2^nrows
    assert (date_array == date_maximum).any()  # Probability false = 1/2^nrows


@pytest.mark.parametrize('minimum,maximum', [("2000", "2001"),
                                             ("2000-01-01", "2001-01-01"),
                                             ("2000-01-01", "2000-01-01")])
def test_generate_year_bounded(minimum, maximum):
    # Given
    column_generator = column.Year(nrows=100, minimum=minimum, maximum=maximum)
    date_minimum = np.datetime64(minimum)
    date_maximum = np.datetime64(maximum)

    # When
    array = column_generator._get_1d_array()
    date_array = np.array(list(map(np.datetime64, array)))
    print(array)
    # Then
    assert np.greater_equal(date_array, date_minimum).all()
    assert np.less_equal(date_array, date_maximum).all()
    assert (date_array == date_minimum).any()  # Probability false = 1/2^nrows
    assert (date_array == date_maximum).any()  # Probability false = 1/2^nrows


@pytest.mark.parametrize('date, format_date, expected', [("2000-01-01", "%Y%m%d", "20000101"),
                                       # ("2100-12-29", "%Y_%m_%d", "2100_12_29"),
                                       # ("1800-01-01", "%Y/%m/%d", "1800/01/01"),
                                        ("2000-01-01", None, "2000-01-01")])
def test_generate_date_format(date, format_date, expected):

    column_generator = column.Date(nrows=100, minimum=date, maximum=date, format=format_date)
    array = column_generator._get_1d_array()

    date_array = np.array(list(map(np.datetime64, array)))
    expected_date = np.datetime64(expected)

    assert np.equal(date_array, expected_date).all()


@pytest.mark.parametrize('date, format_datetime, expected', [("2001-01-01T00:00:00", None, "2001-01-01T00:00:00Z"),
                                             ("2000-01-01T00:00:00", "%Y%m%d%H", "2000010100")
                                             ])
def test_generate_datetime_format(date, format_datetime, expected):

    column_generator = column.Datetime(nrows=100, minimum=date, maximum=date, format=format_datetime)
    array = column_generator._get_1d_array()

    date_array = np.array(list(map(np.datetime64, array)))
    expected_date = np.datetime64(expected)

    assert np.equal(date_array, expected_date).all()


@pytest.mark.parametrize('trueValues,falseValues,expected', [(["true"], ["false"], ["true", "false"]), (["TrUe"], ["fAlSE"], ["TrUe", "fAlSE"]),
                                                             (["true"], None, ["true", "0"]), (None, ["false"], ["1", "false"]),
                                                             (None, None, ["1", "0"]), (["1"], ["0"], ["1", "0"])])
def test_generate_boolean_values(trueValues, falseValues, expected):
    # Given
    column_generator = column.Boolean(nrows=100, trueValues=trueValues, falseValues=falseValues)

    # When
    array = column_generator._get_1d_array()

    # Then
    array_set = set(array)
    assert list(array_set).sort() == expected.sort()
