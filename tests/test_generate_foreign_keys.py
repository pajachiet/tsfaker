import os

import numpy as np
import pytest

from tsfaker.generator.foreign_key import ForeignKeyCatalog, ForeignKeyGenerator

parent_csv_path = os.path.join('tests', 'csv', 'parent.csv')

foreign_keys = [
    ('string_parent', [("str_1",), ("str_2",), ("str_3",)]),
    (('string_parent', 'number_parent'), [("str_1", "1.324"),
                                          ("str_2", "2"),
                                          ("str_3", "-3"),
                                          ]),
    (('number_parent', 'string_parent'), [("1.324", "str_1"),
                                          ("2", "str_2"),
                                          ("-3", "str_3"),
                                          ]),
]


@pytest.mark.parametrize('fields,expected_result', foreign_keys)
def test_read_foreign_key_values(fields, expected_result):
    # Given
    nrows = 10
    foreign_key_catalog = ForeignKeyCatalog(resource_name_to_path_or_schema={'parent': parent_csv_path})
    foreign_key = ForeignKeyGenerator(nrows=nrows, fields=fields, resource_name='parent', resource_fields=fields,
                                      foreign_key_catalog=foreign_key_catalog)
    expected_result = np.array(expected_result)
    expected_first_column_values = set(expected_result[:, 0])

    # When
    actual_result = foreign_key.foreign_key_values
    first_column = foreign_key.get_column(foreign_key.fields[0])
    actual_first_column_values = set(first_column)

    # Then
    assert np.array_equal(expected_result, actual_result)
    assert (nrows,) == first_column.shape
    assert actual_first_column_values <= expected_first_column_values
