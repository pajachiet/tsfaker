import os

import numpy as np
import pytest
from tableschema import Schema

from tsfaker import tstype
from tsfaker.generator.column import tstype_to_generator_class, Enum
from tsfaker.generator.table import TableGenerator


@pytest.mark.parametrize("column_generator_class", tstype_to_generator_class.values())
def test_random_seed_for_column_generator(column_generator_class):
    column_generator_1a = column_generator_class(10, random_seed=30)
    column_generator_1b = column_generator_class(10, random_seed=30)
    column_generator_2 = column_generator_class(10, random_seed=10)

    column_1a = column_generator_1a.get_2d_array()
    column_1b = column_generator_1b.get_2d_array()

    column_2 = column_generator_2.get_2d_array()

    # Then
    assert np.array_equal(column_1a, column_1b)
    assert not np.array_equal(column_1a, column_2)


def test_random_seed_for_enum():
    enum = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k']
    ts_type = 'string'
    column_generator_1a = Enum(10, enum=enum, type=ts_type, random_seed=30)
    column_generator_1b = Enum(10, enum=enum, type=ts_type, random_seed=30)
    column_generator_2 = Enum(10, enum=enum, type=ts_type, random_seed=10)

    column_1a = column_generator_1a.get_2d_array()
    column_1b = column_generator_1b.get_2d_array()
    column_2 = column_generator_2.get_2d_array()

    # Then
    assert np.array_equal(column_1a, column_1b)
    assert not np.array_equal(column_1a, column_2)


@pytest.mark.parametrize('schema_filename', ['implemented_types.json', 'bounded.json', 'enum.json'])
def test_random_seed_for_table_generated_from_schema(schema_filename):
    # Given
    schema_path = os.path.join('tests', 'schemas', schema_filename)
    schema = Schema(schema_path)

    table_generator_1a = TableGenerator(schema, 10, random_seed=30)
    table_generator_1b = TableGenerator(schema, 10, random_seed=30)
    table_generator_2 = TableGenerator(schema, 10, random_seed=10)

    # When
    table_string_1a = table_generator_1a.get_string(pretty=False)
    table_string_1b = table_generator_1b.get_string(pretty=False)
    table_string_2 = table_generator_2.get_string(pretty=False)

    # Then
    assert table_string_1a == table_string_1b
    assert table_string_1a != table_string_2


def test_random_seed_for_table_generated_from_schema_with_foreign_key():
    # Given
    child_schema_path = os.path.join('tests', 'schemas', 'parent_child', 'child1.json')
    resource_name_to_path_or_schema = {'parent': os.path.join('tests', 'csv', 'parent.csv')}

    schema = Schema(child_schema_path)
    table_generator_1a = TableGenerator(schema, 10, resource_name_to_path_or_schema, random_seed=30)
    table_generator_1b = TableGenerator(schema, 10, resource_name_to_path_or_schema, random_seed=30)
    table_generator_2 = TableGenerator(schema, 10, resource_name_to_path_or_schema, random_seed=10)

    # When
    table_string_1a = table_generator_1a.get_string(pretty=False)
    table_string_1b = table_generator_1b.get_string(pretty=False)
    table_string_2 = table_generator_2.get_string(pretty=False)

    # Then
    assert table_string_1a == table_string_1b
    assert table_string_1a != table_string_2


def test_random_seed_makes_differences_between_same_type_column():
    # Given
    schema_path = os.path.join('tests', 'schemas', 'implemented_types_doubled.json')
    schema = Schema(schema_path)

    table_generator = TableGenerator(schema, 10, random_seed=30)

    # When
    df_generated = table_generator.get_dataframe()
    for ts_type in tstype.implemented:
        assert ts_type + '_1' in df_generated.columns
        assert ts_type + '_2' in df_generated.columns
        assert not (df_generated[ts_type + '_1'] == df_generated[ts_type + '_2']).all()

    # Then
