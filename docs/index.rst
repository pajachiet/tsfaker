.. tsfaker documentation master file, created by
   sphinx-quickstart on Tue May 21 18:42:24 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tsfaker's documentation!
===================================

Generate tabular fake data conforming to a `Table Schema <https://frictionlessdata.io/specs/table-schema/>`_

.. toctree::
   :maxdepth: 3

   readme_link


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
